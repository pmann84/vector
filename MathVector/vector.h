#ifndef __MATH_VECTOR_H__
#define __MATH_VECTOR_H__

#include <array>
#include <string>
#include <exception>

// TODO: Implement ==
// TODO: Implement !=
// TODO: Implement Parallel check - opposite direction but not necessarily the same magnitude
// TODO: Implement AntiParallel check - same direction but not necessarily the same magnitude
// TODO: Opposite (plus opposite check)
// TODO: Angle between? acos(dotprod(l, r)/ |l||r|)
// TODO: Cross/Vector Product (2&3 dims only)
// TODO: Scalar triple product - a dot(b x c) (2&3 dims only)

namespace math
{
	template<class TYPE, unsigned int DIM>
	class vector
	{
	public:
		// Constructors
		vector() : vector(0) {}

		explicit vector(TYPE value)
		{
			std::fill(m_data.begin(), m_data.end(), value);
		}

		vector(const vector<TYPE, DIM>& vector)
		{
			std::copy(vector.begin(), vector.end(), begin());
		}

		vector(std::initializer_list<TYPE> data)
		{
			if (data.size() > DIM)
			{
				throw XVectorDimensionIncorrect(DIM, data.size());
			}
			std::copy(data.begin(), data.end(), begin());
		}

		// Random Access Operators
		TYPE& operator[] (unsigned int n)
		{
			return m_data[n];
		}

		const TYPE& operator[] (unsigned int n) const
		{
			return m_data[n];
		}

		// Iterators
		typename std::array<TYPE, DIM>::iterator begin()
		{
			return m_data.begin();
		}

		typename std::array<TYPE, DIM>::const_iterator begin() const
		{
			return m_data.begin();
		}

		typename std::array<TYPE, DIM>::iterator end()
		{
			return m_data.end();
		}

		typename std::array<TYPE, DIM>::const_iterator end() const
		{
			return m_data.end();
		}

		// Length
		unsigned int size() const
		{
			return DIM;
		}

		// Validation
		int is_equal_length(const vector& vec)
		{
			return size() == vec.size();
		}

		// Assignment operators
		vector<TYPE, DIM>& operator=(const vector<TYPE, DIM>& other)
		{
			for (unsigned int i = 0; i < DIM; ++i)
			{
				m_data[i] = other[i];
			}
			return *this;
		}

		vector<TYPE, DIM>& operator=(const std::vector<TYPE>& vec)
		{
			size_t min_dim = std::min(DIM, vec.size());
			for (size_t i = 0; i < min_dim; ++i) 
			{
				m_data[i] = vec[i];
			}
			return *this;
		}

		vector<TYPE, DIM>& operator=(const std::initializer_list<TYPE>& list)
		{
			size_t min_dim = std::min(DIM, list.size());
			size_t i = 0;
			for (auto list_it = list.begin(); i < min_dim; ++i, ++list_it) 
			{
				m_data[i] = *list_it;
			}
			return *this;
		}

		// Arithmetic Operators for other vectors
		vector<TYPE, DIM> operator+(const vector<TYPE, DIM>& rhs)
		{
			vector<TYPE, DIM> ret(*this);
			ret += rhs;
			return ret;
		}
		
		vector<TYPE, DIM>& operator+=(const vector<TYPE, DIM>& rhs)
		{
			for (unsigned int i = 0; i < DIM; ++i)
			{
				m_data[i] += rhs[i];
			}
			return *this;
		}
		
		vector<TYPE, DIM> operator-(const vector<TYPE, DIM>& rhs)
		{
			vector<TYPE, DIM> ret(*this);
			ret -= rhs;
			return ret;
		}
		
		vector<TYPE, DIM>& operator-=(const vector<TYPE, DIM>& rhs)
		{
			for (unsigned int i = 0; i < DIM; ++i)
			{
				m_data[i] -= rhs[i];
			}
			return *this;
		}

		friend vector<TYPE, DIM> operator*(vector lhs, const TYPE& rhs)
		{
			return lhs *= rhs;
		}

		friend vector<TYPE, DIM> operator*(const TYPE& lhs, vector rhs)
		{
			return rhs *= lhs;
		}

		vector<TYPE, DIM>& operator*=(const TYPE& rhs)
		{
			for (unsigned int i = 0; i < DIM; ++i)
			{
				m_data[i] *= rhs;
			}
			return *this;
		}

		vector<TYPE, DIM> operator*(const vector<TYPE, DIM>& rhs)
		{
			vector<TYPE, DIM> ret(*this);
			ret *= rhs;
			return ret;
		}

		vector<TYPE, DIM>& operator*=(const vector<TYPE, DIM>& rhs)
		{
			for (unsigned int i = 0; i < DIM; ++i)
			{
				m_data[i] *= rhs[i];
			}
			return *this;
		}

		friend vector<TYPE, DIM> operator/(vector lhs, const TYPE& rhs)
		{
			return lhs /= rhs;
		}

		vector<TYPE, DIM>& operator/=(const TYPE& rhs)
		{
			for (unsigned int i = 0; i < DIM; ++i)
			{
				m_data[i] /= rhs;
			}
			return *this;
		}

		vector<TYPE, DIM> operator/(const vector<TYPE, DIM>& rhs)
		{
			vector<TYPE, DIM> ret(*this);
			ret /= rhs;
			return ret;
		}

		vector<TYPE, DIM>& operator/=(const vector<TYPE, DIM>& rhs)
		{
			for (unsigned int i = 0; i < DIM; ++i)
			{
				m_data[i] /= rhs[i];
			}
			return *this;
		}

		// Mathematical vector operations
		TYPE norm_squared()
		{
			// The euclidean norm squared - the sum of the squares of all the vector elements
			// This is basically just the inner product with itself
			return inner_product(*this, *this);
		}
		
		TYPE norm()
		{
			// Also known as length or magnitude
			// The euclidean norm - defined as the square root of sum of the squares of all the vector elements
			// Physically this is the length of the vector
			return std::sqrt(norm_squared());
		}
		
		void normalise()
		{
			auto magnitude = norm();
			for (auto i = 0; i<DIM; ++i)
			{
				m_data[i] /= magnitude;
			}
		}
		
		// Dot/Scalar Product a . b, this is the Inner Product as defined for Rn, 
		// which is a way to multiply vectors together that results in a scalar
		friend TYPE inner_product(const vector<TYPE, DIM>& lhs, const vector<TYPE, DIM>& rhs)
		{
			TYPE result = static_cast<TYPE>(0);
			for (int i = 0; i < lhs.size(); ++i)
			{
				result += lhs[i] * rhs[i];
			}
			return result;
		}

	private:
		std::array<TYPE, DIM> m_data;
	};

	// Predefined length vectors
	template <class TYPE>
	using vector2 = vector<TYPE, 2>;

	template <class TYPE>
	using vector3 = vector<TYPE, 3>;

	template <class TYPE>
	using vector4 = vector<TYPE, 4>;

	// Vector Exceptions
	class XVectorDimensionIncorrect : public std::exception
	{
	public:
		XVectorDimensionIncorrect(const int dim, const int expectedMaxDim)
			: m_msg(std::string("Expected dimension of size less than ")
				+ std::to_string(expectedMaxDim) + std::string(" but got size ")
				+ std::to_string(dim))
		{
		}

		const char* what() const throw() override
		{
			return m_msg.c_str();
		}

	private:
		std::string m_msg;
	};
}
#endif // __MATH_VECTOR_H__