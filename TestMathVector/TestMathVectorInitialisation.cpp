#include "gtest/gtest.h"
#include "gmock/gmock.h"

#include "../MathVector/vector.h"

TEST(TestMathVectorInitialisation, InitialiseToZero)
{
	const unsigned int vec_size = 10;
	math::vector<double, vec_size> vec;
	for(int i=0; i<vec_size; ++i)
	{
		EXPECT_DOUBLE_EQ(vec[i], 0.0);
	}
}

TEST(TestMathVectorInitialisation, InitialiseToConstant)
{
	const unsigned int vec_size = 10;
	double init_value = 54.2;
	math::vector<double, vec_size> vec(init_value);
	for (int i = 0; i<vec_size; ++i)
	{
		EXPECT_DOUBLE_EQ(vec[i], init_value);
	}
}