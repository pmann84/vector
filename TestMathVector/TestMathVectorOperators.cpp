#include "gtest/gtest.h"
#include "gmock/gmock.h"

#include "../MathVector/vector.h"

TEST(TestMathVectorOperators, TestPlusOperatorReturnsCorrectValues)
{
	const unsigned int vec_size = 4;
	math::vector<double, vec_size> vec1({ 1.0, 2.0, 3.0, 4.0 }), vec2({ 4.0, 3.0, 2.0, 1.0 });
	math::vector<double, vec_size> add_result = vec1 + vec2;
	for (int i = 0; i<vec_size; ++i)
	{	
		EXPECT_DOUBLE_EQ(add_result[i], 5.0);
	}
	// Check original vectors are unmodified
	EXPECT_DOUBLE_EQ(vec1[0], 1.0);
	EXPECT_DOUBLE_EQ(vec1[1], 2.0);
	EXPECT_DOUBLE_EQ(vec1[2], 3.0);
	EXPECT_DOUBLE_EQ(vec1[3], 4.0);
	EXPECT_DOUBLE_EQ(vec2[0], 4.0);
	EXPECT_DOUBLE_EQ(vec2[1], 3.0);
	EXPECT_DOUBLE_EQ(vec2[2], 2.0);
	EXPECT_DOUBLE_EQ(vec2[3], 1.0);
}

TEST(TestMathVectorOperators, TestCompoundPlusOperatorReturnsCorrectValues)
{
	const unsigned int vec_size = 4;
	math::vector<double, vec_size> vec1({ 1.0, 2.0, 3.0, 4.0 }), vec2({ 4.0, 3.0, 2.0, 1.0 });
	vec1 += vec2;
	for (int i = 0; i<vec_size; ++i)
	{
		EXPECT_DOUBLE_EQ(vec1[i], 5.0);
	}
	// Check second vector are unmodified
	EXPECT_DOUBLE_EQ(vec2[0], 4.0);
	EXPECT_DOUBLE_EQ(vec2[1], 3.0);
	EXPECT_DOUBLE_EQ(vec2[2], 2.0);
	EXPECT_DOUBLE_EQ(vec2[3], 1.0);
}

TEST(TestMathVectorOperators, TestMinusOperatorReturnsCorrectValues)
{
	const unsigned int vec_size = 4;
	math::vector<double, vec_size> vec1({ 1.0, 2.0, 3.0, 4.0 }), vec2({ 4.0, 3.0, 2.0, 1.0 });
	math::vector<double, vec_size> minus_result = vec1 - vec2;
	// Check result has correct values
	EXPECT_DOUBLE_EQ(minus_result[0], -3.0);
	EXPECT_DOUBLE_EQ(minus_result[1], -1.0);
	EXPECT_DOUBLE_EQ(minus_result[2], 1.0);
	EXPECT_DOUBLE_EQ(minus_result[3], 3.0);
	// Check original vectors are unmodified
	EXPECT_DOUBLE_EQ(vec1[0], 1.0);
	EXPECT_DOUBLE_EQ(vec1[1], 2.0);
	EXPECT_DOUBLE_EQ(vec1[2], 3.0);
	EXPECT_DOUBLE_EQ(vec1[3], 4.0);
	EXPECT_DOUBLE_EQ(vec2[0], 4.0);
	EXPECT_DOUBLE_EQ(vec2[1], 3.0);
	EXPECT_DOUBLE_EQ(vec2[2], 2.0);
	EXPECT_DOUBLE_EQ(vec2[3], 1.0);
}

TEST(TestMathVectorOperators, TestCompoundMinusOperatorReturnsCorrectValues)
{
	const unsigned int vec_size = 4;
	math::vector<double, vec_size> vec1({ 1.0, 2.0, 3.0, 4.0 }), vec2({ 4.0, 3.0, 2.0, 1.0 });
	vec1 -= vec2;
	// Check result has correct values
	EXPECT_DOUBLE_EQ(vec1[0], -3.0);
	EXPECT_DOUBLE_EQ(vec1[1], -1.0);
	EXPECT_DOUBLE_EQ(vec1[2], 1.0);
	EXPECT_DOUBLE_EQ(vec1[3], 3.0);
	// Check second vector are unmodified
	EXPECT_DOUBLE_EQ(vec2[0], 4.0);
	EXPECT_DOUBLE_EQ(vec2[1], 3.0);
	EXPECT_DOUBLE_EQ(vec2[2], 2.0);
	EXPECT_DOUBLE_EQ(vec2[3], 1.0);
}

TEST(TestMathVectorOperators, TestMultiplicationOperatorReturnsCorrectValues)
{
	const unsigned int vec_size = 4;
	math::vector<double, vec_size> vec1({ 1.0, 2.0, 3.0, 4.0 });
	math::vector<double, vec_size> multiplication_result = vec1 * 2.0;
	// Check result has correct values
	EXPECT_DOUBLE_EQ(multiplication_result[0], 2.0);
	EXPECT_DOUBLE_EQ(multiplication_result[1], 4.0);
	EXPECT_DOUBLE_EQ(multiplication_result[2], 6.0);
	EXPECT_DOUBLE_EQ(multiplication_result[3], 8.0);
	// Check original vectors are unmodified
	EXPECT_DOUBLE_EQ(vec1[0], 1.0);
	EXPECT_DOUBLE_EQ(vec1[1], 2.0);
	EXPECT_DOUBLE_EQ(vec1[2], 3.0);
	EXPECT_DOUBLE_EQ(vec1[3], 4.0);
}

TEST(TestMathVectorOperators, TestMultiplicationOperatorInvertedReturnsCorrectValues)
{
	const unsigned int vec_size = 4;
	math::vector<double, vec_size> vec1({ 1.0, 2.0, 3.0, 4.0 });
	math::vector<double, vec_size> multiplication_result = 2.0 * vec1;
	// Check result has correct values
	EXPECT_DOUBLE_EQ(multiplication_result[0], 2.0);
	EXPECT_DOUBLE_EQ(multiplication_result[1], 4.0);
	EXPECT_DOUBLE_EQ(multiplication_result[2], 6.0);
	EXPECT_DOUBLE_EQ(multiplication_result[3], 8.0);
	// Check original vectors are unmodified
	EXPECT_DOUBLE_EQ(vec1[0], 1.0);
	EXPECT_DOUBLE_EQ(vec1[1], 2.0);
	EXPECT_DOUBLE_EQ(vec1[2], 3.0);
	EXPECT_DOUBLE_EQ(vec1[3], 4.0);
}

TEST(TestMathVectorOperators, TestCompoundMultiplicationOperatorReturnsCorrectValues)
{
	const unsigned int vec_size = 4;
	math::vector<double, vec_size> vec1({ 1.0, 2.0, 3.0, 4.0 });
	vec1 *= 2.0;
	// Check result has correct values
	EXPECT_DOUBLE_EQ(vec1[0], 2.0);
	EXPECT_DOUBLE_EQ(vec1[1], 4.0);
	EXPECT_DOUBLE_EQ(vec1[2], 6.0);
	EXPECT_DOUBLE_EQ(vec1[3], 8.0);
}

TEST(TestMathVectorOperators, TestVectorMultiplicationOperatorReturnsCorrectValues)
{
	const unsigned int vec_size = 4;
	math::vector<double, vec_size> vec1({ 1.0, 2.0, 3.0, 4.0 }), vec2({ 4.0, 3.0, 2.0, 1.0 });
	math::vector<double, vec_size> add_result = vec1 * vec2;
	EXPECT_DOUBLE_EQ(add_result[0], 4.0);
	EXPECT_DOUBLE_EQ(add_result[1], 6.0);
	EXPECT_DOUBLE_EQ(add_result[2], 6.0);
	EXPECT_DOUBLE_EQ(add_result[3], 4.0);
	// Check original vectors are unmodified
	EXPECT_DOUBLE_EQ(vec1[0], 1.0);
	EXPECT_DOUBLE_EQ(vec1[1], 2.0);
	EXPECT_DOUBLE_EQ(vec1[2], 3.0);
	EXPECT_DOUBLE_EQ(vec1[3], 4.0);
	EXPECT_DOUBLE_EQ(vec2[0], 4.0);
	EXPECT_DOUBLE_EQ(vec2[1], 3.0);
	EXPECT_DOUBLE_EQ(vec2[2], 2.0);
	EXPECT_DOUBLE_EQ(vec2[3], 1.0);
}

TEST(TestMathVectorOperators, TestVectorCompoundMultiplicationOperatorReturnsCorrectValues)
{
	const unsigned int vec_size = 4;
	math::vector<double, vec_size> vec1({ 1.0, 2.0, 3.0, 4.0 }), vec2({ 4.0, 3.0, 2.0, 1.0 });
	vec1 *= vec2;
	EXPECT_DOUBLE_EQ(vec1[0], 4.0);
	EXPECT_DOUBLE_EQ(vec1[1], 6.0);
	EXPECT_DOUBLE_EQ(vec1[2], 6.0);
	EXPECT_DOUBLE_EQ(vec1[3], 4.0);
	// Check original vectors are unmodified
	EXPECT_DOUBLE_EQ(vec2[0], 4.0);
	EXPECT_DOUBLE_EQ(vec2[1], 3.0);
	EXPECT_DOUBLE_EQ(vec2[2], 2.0);
	EXPECT_DOUBLE_EQ(vec2[3], 1.0);
}

TEST(TestMathVectorOperators, TestDivisionOperatorReturnsCorrectValues)
{
	const unsigned int vec_size = 4;
	math::vector<double, vec_size> vec1({ 1.0, 2.0, 3.0, 4.0 });
	math::vector<double, vec_size> multiplication_result = vec1 / 2.0;
	// Check result has correct values
	EXPECT_DOUBLE_EQ(multiplication_result[0], 0.5);
	EXPECT_DOUBLE_EQ(multiplication_result[1], 1.0);
	EXPECT_DOUBLE_EQ(multiplication_result[2], 1.5);
	EXPECT_DOUBLE_EQ(multiplication_result[3], 2.0);
	// Check original vectors are unmodified
	EXPECT_DOUBLE_EQ(vec1[0], 1.0);
	EXPECT_DOUBLE_EQ(vec1[1], 2.0);
	EXPECT_DOUBLE_EQ(vec1[2], 3.0);
	EXPECT_DOUBLE_EQ(vec1[3], 4.0);
}

TEST(TestMathVectorOperators, TestCompoundDivisionOperatorReturnsCorrectValues)
{
	const unsigned int vec_size = 4;
	math::vector<double, vec_size> vec1({ 1.0, 2.0, 3.0, 4.0 });
	vec1 /= 2.0;
	// Check result has correct values
	EXPECT_DOUBLE_EQ(vec1[0], 0.5);
	EXPECT_DOUBLE_EQ(vec1[1], 1.0);
	EXPECT_DOUBLE_EQ(vec1[2], 1.5);
	EXPECT_DOUBLE_EQ(vec1[3], 2.0);
}

TEST(TestMathVectorOperators, TestVectorDivisionOperatorReturnsCorrectValues)
{
	const unsigned int vec_size = 4;
	math::vector<double, vec_size> vec1({ 1.0, 2.0, 3.0, 4.0 }), vec2({ 4.0, 4.0, 2.0, 1.0 });
	math::vector<double, vec_size> div_result = vec1 / vec2;
	EXPECT_DOUBLE_EQ(div_result[0], 0.25);
	EXPECT_DOUBLE_EQ(div_result[1], 0.5);
	EXPECT_DOUBLE_EQ(div_result[2], 1.5);
	EXPECT_DOUBLE_EQ(div_result[3], 4.0);
	// Check original vectors are unmodified
	EXPECT_DOUBLE_EQ(vec1[0], 1.0);
	EXPECT_DOUBLE_EQ(vec1[1], 2.0);
	EXPECT_DOUBLE_EQ(vec1[2], 3.0);
	EXPECT_DOUBLE_EQ(vec1[3], 4.0);
	EXPECT_DOUBLE_EQ(vec2[0], 4.0);
	EXPECT_DOUBLE_EQ(vec2[1], 4.0);
	EXPECT_DOUBLE_EQ(vec2[2], 2.0);
	EXPECT_DOUBLE_EQ(vec2[3], 1.0);
}

TEST(TestMathVectorOperators, TestVectorCompoundDivisionOperatorReturnsCorrectValues)
{
	const unsigned int vec_size = 4;
	math::vector<double, vec_size> vec1({ 1.0, 2.0, 3.0, 4.0 }), vec2({ 4.0, 4.0, 2.0, 1.0 });
	vec1 /= vec2;
	EXPECT_DOUBLE_EQ(vec1[0], 0.25);
	EXPECT_DOUBLE_EQ(vec1[1], 0.5);
	EXPECT_DOUBLE_EQ(vec1[2], 1.5);
	EXPECT_DOUBLE_EQ(vec1[3], 4.0);
	// Check original vectors are unmodified
	EXPECT_DOUBLE_EQ(vec2[0], 4.0);
	EXPECT_DOUBLE_EQ(vec2[1], 4.0);
	EXPECT_DOUBLE_EQ(vec2[2], 2.0);
	EXPECT_DOUBLE_EQ(vec2[3], 1.0);
}