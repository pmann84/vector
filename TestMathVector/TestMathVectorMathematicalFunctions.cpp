#include "gtest/gtest.h"
#include "gmock/gmock.h"

#include "../MathVector/vector.h"

TEST(TestMathVectorMathematicalFunctions, TestNormSquaredReturnsCorrectValue)
{
	const unsigned int vec_size = 5;
	math::vector<double, vec_size> vec({1.0, 2.0, 3.0, 2.0, 1.0});
	EXPECT_DOUBLE_EQ(vec.norm_squared(), 19.0);
}

TEST(TestMathVectorMathematicalFunctions, TestNormReturnsCorrectValue)
{
	const unsigned int vec_size = 5;
	math::vector<double, vec_size> vec({ 1.0, 2.0, 3.0, 2.0, 1.0 });
	EXPECT_DOUBLE_EQ(vec.norm(), std::sqrt(19.0));
}