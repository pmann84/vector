# README #

### What is this repository for? ###

This repository contains a basic implementation of a mathematical vector in C++. It comes with arithemetic operators implemented and some other mathematical functionality:

* Addition
* Subtraction
* Multiplication (scalar and component-wise)
* Division (scalar and component-wise)
* Assignment (from vector and initialiser list)
* Normalisation 
* Inner Product
* Easy 2, 3 and 4 vector aliases

### How do I get set up? ###

This is a header only library so just include this in your project

### Who do I talk to? ###

* Repo owner